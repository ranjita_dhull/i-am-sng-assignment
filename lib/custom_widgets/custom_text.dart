import 'package:flutter/material.dart';
import 'package:iamsng/common/constants/dimens_constants.dart';
import 'package:iamsng/common/constants/font_constants.dart';
import 'package:iamsng/common/theme/custom_theme.dart';




class CustomText extends StatelessWidget {
  String strText;
  double fontSize;
  Color color;
  FontStyle fontStyle;
  FontWeight fontWeight;
  TextDecoration textDecoration;
  String fontFamily;
  TextOverflow overflow;
  TextAlign textAlign;
  bool applyThemeStyle;
  var maxLines;
  TextStyle textstyle;


  CustomText(this.strText,
      {this.fontSize = FONT_SIZE_15,
        this.color = CustomTheme.colorGreyText,
        this.fontStyle = FontStyle.normal,
        this.fontWeight = ROBOTO_REGULAR_WEIGHT,
        this.overflow = TextOverflow.clip,
        this.textAlign = TextAlign.center,
        this.maxLines = null,
        this.textDecoration = TextDecoration.none,
        this.applyThemeStyle,
        this.textstyle,
        this.fontFamily="fonts/Roboto"});


  @override
  Widget build(BuildContext context) {
    return Text(
      strText,
      overflow: overflow,
      textAlign: textAlign,
      maxLines: maxLines,
      style: (textstyle==null)?TextStyle(
          decoration: textDecoration,
          color: color,
          fontSize: fontSize,
          fontFamily: fontFamily,
          fontStyle: fontStyle,
          fontWeight: fontWeight
      ):textstyle,
    );
  }
}