import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iamsng/localization/app_localization/app_localization.dart';

class InfoAlertDialog {
  static infoAlert(BuildContext context, String message, String title) {
    return
        //cupertino, default alert dialog
        showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(message),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text(AppLocalizations.of(context).commonButtonText.assToCart),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    );
  }

  static infoAlertWithCallBack(BuildContext context, String message, String title, OnPressCallbackFunction _onPressCallback) {
    return
        //cupertino, default alert dialog
        showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(message),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text(AppLocalizations.of(context).commonButtonText.assToCart),
            onPressed: () {
              Future.delayed(Duration(milliseconds: 100), () {
                Navigator.of(context).pop();
              });
            },
          ),
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text(AppLocalizations.of(context).commonButtonText.viewAll),
            onPressed: () {
              Future.delayed(Duration(milliseconds: 100), () {
                Navigator.of(context).pop();
              });
              _onPressCallback();
            },
          )
        ],
      ),
    );
  }
}

typedef OnPressCallbackFunction = void Function();
