import 'package:flutter/material.dart';
import 'package:iamsng/screens/lib/detail_module/detail.dart';
import 'package:iamsng/start.dart';



class MyRoutes {

  static String INITIAL_ROUTE =
      '/'; // start screen where we fetched the data
  static const String DETAIL_ROOT = '/main_repository';



  static Map<String, WidgetBuilder> routes() {
    return {
      INITIAL_ROUTE: (context) => StartApp(),
      DETAIL_ROOT: (context) => Detail(),


    };
  }
}