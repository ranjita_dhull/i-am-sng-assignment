import 'package:flutter/material.dart';
import 'package:iamsng/blocs/respository_bloc.dart';

// helps to provide the bloc
class RepositoryProvider extends InheritedWidget {
  final RepositoryBloc repositoryBloc;

  RepositoryProvider({@required this.repositoryBloc, Widget child, stayyBloc}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static RepositoryBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(RepositoryProvider) as RepositoryProvider).repositoryBloc;
  }
}
