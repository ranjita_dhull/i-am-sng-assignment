
import 'package:flutter/material.dart';
import 'package:iamsng/api/repository/repository_api_provider.dart';
import 'package:iamsng/blocs/respository_bloc.dart';
import 'package:iamsng/common/constants/app_constants.dart';
import 'package:iamsng/screens/lib/home_module/home.dart';
import 'package:geolocator/geolocator.dart';
import 'package:iamsng/utils/connection.dart';

import 'custom_widgets/default_alert_dialog.dart';
import 'localization/app_localization/app_localization.dart';

class StartApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StartAppState();
  }
}

class _StartAppState extends State<StartApp> {


  @override
  void initState() {
    super.initState();

    NetworkCheck.
    internal().getConnectivityStatus(context, showSnackBar: true).then((networkConnected) {
      if (networkConnected) {
        Geolocator()
            .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
            .then((value) {
          print("---------------------------------> lat=${value
              .latitude}&lon=${value.longitude}");

          RepositoryApiProvider.internal()
              .getCall(
              'api/v2.1/geocode?lat=${value.latitude}&lon=${value.longitude}')
              .then((data) {
            if (data.status == SUCCESS) {

              RepositoryBloc
                  .getInstance()
                  .updateRespository
                  .add(data);
            } else {
              InfoAlertDialog.infoAlert(context, AppLocalizations.of(context).commonLocalization.appLocalizations_Labels_Common_Titles.somethingWentWrongTitle, AppLocalizations.of(context).commonLocalization.appLocalizations_Labels_Common_Titles.popularTitle);
            }
          });
        });
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Home();
  }

}
