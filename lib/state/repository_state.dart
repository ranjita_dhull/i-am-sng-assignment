

import 'package:iamsng/common/constants/app_constants.dart';
import 'package:iamsng/model/restaurent_response_model.dart';

class RepositoryState {

  RestaurantsResponseModel responseModel;
  int status = WAITING;
  List<NearbyRestaurant> popularRestaurants= List<NearbyRestaurant>();
  List<CuisineWiseClassification> cuisineWiseClassificationList= List<CuisineWiseClassification>();
  int currentCuisineIndex = 0;

  clear() {
    responseModel=null;
    status = WAITING;
    popularRestaurants.clear();
    cuisineWiseClassificationList.clear();
    currentCuisineIndex=0;
  }
}