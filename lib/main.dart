import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iamsng/routes.dart';

import 'app_build_variants/app_config.dart';
import 'common/constants/local_constants.dart';
import 'localization/app_localization/app_localization.dart';


Future main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);


  var configuredApp = AppConfig(
    child: MyApplication(),
  );



  runZoned<Future<Null>>(() async {
    runApp(configuredApp);
  });


}

class MyApplication extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    AppConfig config = AppConfig.of(context);
    print('Calling main class first');


    return MaterialApp(
      debugShowCheckedModeBanner: false,
      locale: Locale(LOCALE_ENG),
      title: 'I AM SNG ASSIGNMENT',
      theme: config.themeData,
      routes: MyRoutes.routes(),
      localizationsDelegates: [
        AppLocalizationsDelegate(),
      ],
      supportedLocales: [
        const Locale(LOCALE_ENG), // English
      ],
    );
  }
}
