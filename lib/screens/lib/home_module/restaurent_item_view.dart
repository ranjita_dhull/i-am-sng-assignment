import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iamsng/blocs/respository_bloc.dart';
import 'package:iamsng/common/constants/app_constants.dart';
import 'package:iamsng/common/utils/app_utils.dart';
import 'package:iamsng/model/restaurent_response_model.dart';
import 'package:iamsng/routes.dart';
import 'package:iamsng/state/repository_state.dart';

class RestaurentItemView extends StatefulWidget {
  int _index;
  List<NearbyRestaurant> _restaurantsList;
  int _listType;
  AsyncSnapshot<RepositoryState> _snapshot;

  RestaurentItemView(this._index, this._restaurantsList, this._listType, this._snapshot);

  @override
  _RestaurentItemViewState createState() => _RestaurentItemViewState();
}

class _RestaurentItemViewState extends State<RestaurentItemView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right:30.0),
      child: Transform.translate(
        offset: Offset(0.0, 25.0),
        child:
        // Adobe XD layer: 'Food infor 1' (group)
        InkWell(
          onTap:  (){_onRestaurentItemTap(widget._restaurantsList[widget._index].restaurant, context);},
          child: Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(16.0, 8.0),
                child:
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 168.0,
                  height: 190.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(24.0),
                    color: const Color(0xffffffff),
                    boxShadow: [
                      BoxShadow(
                        color: const Color(0x1a000000),
                        offset: Offset(8, 16),
                        blurRadius: 32,
                      ),
                    ],
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(136.0, 16.0),
                child:
                // Adobe XD layer: 'Like' (group)
                InkWell(
                  onTap: (){
                    print("Heart tapped");

                    if(widget._listType==NEAR_YOU) {
                      widget._snapshot.data.cuisineWiseClassificationList[widget._snapshot.data.currentCuisineIndex].nearbyRestaurants[
                      widget._index].restaurant.isFav = true;

                    }else {
                      widget._snapshot.data.cuisineWiseClassificationList[widget._snapshot.data.currentCuisineIndex].popularRestaurants[widget._index]
                          .restaurant.isFav = true;
                    }
                    RepositoryBloc
                        .getInstance()
                        .updateRespository
                        .add(widget._snapshot.data);
                  },
                  child: Stack(
                    children: <Widget>[
                      // Adobe XD layer: 'Rectangle' (shape)
                      Container(
                        width: 40.0,
                        height: 32.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(2.0),
                            topRight: Radius.circular(20.0),
                            bottomRight: Radius.circular(2.0),
                            bottomLeft: Radius.circular(20.0),
                          ),
                          color: const Color(0xff99adff),
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(8.0, 4.0),
                        child:
                        // Adobe XD layer: 'Heart' (group)
                        Stack(
                          children: <Widget>[
                            // Adobe XD layer: 'Shape' (shape)
                            SvgPicture.string(
                              (widget._restaurantsList[widget._index].restaurant.isFav)?_svg_qqrib0:_svg_q5rs9t,
                              allowDrawingOutsideViewBox: true,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // Adobe XD layer: 'twenty20_3998e4bc-f…' (group)
              Stack(
                children: <Widget>[
                  // Adobe XD layer: 'Mask' (shape)
                  Container(
                    width: 105.0,
                    height: 105.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.elliptical(52.5, 52.5)),
                      color: const Color(0xffd8d8d8),
                    ),
                  ),
                  // Adobe XD layer: 'twenty20_3998e4bc-f…' (group)
                  Stack(
                    children: <Widget>[
                      Container(
                        width: 105.0,
                        height: 105.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.elliptical(52.5, 52.5)),
                          color: const Color(0xffd8d8d8),
                        ),
                      ),
                      Transform.translate(
                        //offset: Offset(-1.01, -5.23),
                        offset: Offset(0.0, 0.0),
                        child:
                        // Adobe XD layer: 'twenty20_3998e4bc-f…' (shape)
                        CachedNetworkImage(
                          imageUrl: widget._restaurantsList[widget._index].restaurant.featuredImage,
                          placeholder: (context, url) => const CircleAvatar(
                            backgroundColor: const Color(0xffd8d8d8),
                            radius: 55,
                            child:CircularProgressIndicator()
                          ),
                          imageBuilder: (context, image) => CircleAvatar(
                            radius: 55,
                            backgroundColor: const Color(0xffd8d8d8),
                            child: CircleAvatar(
                                radius: 52,
                                backgroundImage: image,
                          ),
                        ),
                        ),
                      ),
                      // Adobe XD layer: 'Mask' (shape)

                    ],
                  ),
                ],
              ),
              Transform.translate(
                offset: Offset(35.0, 113.0),
                child:
                // Adobe XD layer: 'Infor' (group)
                Stack(
                  children: <Widget>[
                    Transform.translate(
                      offset: Offset(0.0, 43.0),
                      child:
                      // Adobe XD layer: 'Fresh hamburger with' (text)
                      SizedBox(
                        width: 131.0,
                        height: 26.0,
                        child: Text(
                          (widget._listType==NEAR_YOU)?widget._restaurantsList[widget._index].restaurant.location.address:
                          ' ${widget._restaurantsList[widget._index].restaurant.userRating.aggregateRating} || ${widget._restaurantsList[widget._index].restaurant.userRating.votes} Votes',
                          style: TextStyle(
                            fontFamily: 'fonts/Montserrat-Regular',
                            fontSize: (widget._listType==NEAR_YOU)?10:15,
                            color: (widget._listType==NEAR_YOU)?const Color(0xff656565):const Color(0xff878787),
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    // Adobe XD layer: '$30.00' (text)
                    Text(
                      '${widget._restaurantsList[widget._index].restaurant.currency} ${widget._restaurantsList[widget._index].restaurant.averageCostForTwo}',
                      style: TextStyle(
                        fontFamily: 'fonts/Montserrat-SemiBold',
                        fontSize: 14,
                        color: const Color(0xff365eff),
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Transform.translate(
                      offset: Offset(0.0, 20.0),
                      child:
                      // Adobe XD layer: 'Chicken Hamburger' (text)
                      SizedBox(
                        width: 131.0,
                        height: 20.0,
                        child: Text(
                          widget._restaurantsList[widget._index].restaurant.name,
                          style: TextStyle(
                            fontFamily: 'fonts/Roboto',
                            fontSize: 14,
                            color: const Color(0xff373737),
                          ),
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _onRestaurentItemTap(Restaurant restaurant, BuildContext _context) {
    AppUtils.NAVIGATOR_UTILS.navigatorPushedName(_context, MyRoutes.DETAIL_ROOT,dataToBeSend: restaurant);
  }
}

//blue heart
const String _svg_qqrib0 =
    '<svg viewBox="0.0 0.0 24.0 24.0" ><path  d="M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /><path transform="translate(3.0, 4.5)" d="M 13.5 0 C 11.89000034332275 0 10.00800037384033 1.825000047683716 9 3 C 7.992000102996826 1.825000047683716 6.110000133514404 0 4.5 0 C 1.651000022888184 0 0 2.221999883651733 0 5.050000190734863 C 0 8.182999610900879 3 11.5 9 15 C 15 11.5 18 8.25 18 5.25 C 18 2.421999931335449 16.34900093078613 0 13.5 0 Z" fill="#365eff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
//
const String _svg_9sgrmi =
    '<svg viewBox="49.6 8.2 1.0 1.0" ><path transform="translate(49.65, 8.16)" d="M 0 0 L 0.7991506457328796 0 L 0.7991506457328796 0.7991506457328796 L 0 0.7991506457328796 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
//white heart
const String _svg_q5rs9t =
    '<svg viewBox="0.0 0.0 24.0 24.0" ><path  d="M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /><path transform="translate(3.0, 4.5)" d="M 13.5 0 C 11.89000034332275 0 10.00800037384033 1.825000047683716 9 3 C 7.992000102996826 1.825000047683716 6.110000133514404 0 4.5 0 C 1.651000022888184 0 0 2.221999883651733 0 5.050000190734863 C 0 8.182999610900879 3 11.5 9 15 C 15 11.5 18 8.25 18 5.25 C 18 2.421999931335449 16.34900093078613 0 13.5 0 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';

