import 'package:flutter/material.dart';
import 'package:iamsng/blocs/respository_bloc.dart';
import 'package:iamsng/state/repository_state.dart';

class CusinesNameItemView extends StatelessWidget {
  int _index;
  int _currentIndex;
  List<String> _topCuisines;
  AsyncSnapshot<RepositoryState> _snapshot;

  CusinesNameItemView(this._index,this._currentIndex, this._topCuisines, this._snapshot);

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.only(right:8.0),
      child: InkWell(
        onTap: (){

          _snapshot.data.currentCuisineIndex=_index;

          RepositoryBloc
              .getInstance()
              .updateRespository
              .add(_snapshot.data);
        },
        child: Stack(
            children: <Widget>[
              // Adobe XD layer: 'Group' (group)
              Container(
                // width: 90.0,
                height: 34.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(16.0),
                    bottomLeft: Radius.circular(16.0),
                  ),
                  color: (_index==_currentIndex)?const Color(0xffedf0ff):Colors.transparent,
                ),
                padding: const EdgeInsets.symmetric(horizontal:5.0),
                alignment: Alignment.center,
                child: Text(
                  _topCuisines[_index],
                  style: TextStyle(
                    fontFamily: 'fonts/Montserrat-Medium',
                    fontSize: 12,
                    color: (_index==_currentIndex)?const Color(0xff365eff):const Color(0xff656565),
                    height: 1.5,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
              )]),
      ),
    );
  }
}
