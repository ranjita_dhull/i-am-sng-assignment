import 'package:flutter/material.dart';
import 'package:iamsng/localization/app_localization/app_localization.dart';

class QuantityView extends StatelessWidget {
  int quatityCount=1;

  @override
  Widget build(BuildContext context) {

    print("GetQuantityView on build ${quatityCount}");
    return Transform.translate(
      offset: Offset(32.0, MediaQuery
          .of(context)
          .size
          .height / 1.4),
      child:
      // Adobe XD layer: 'Qty' (group)
      Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(167.0, -13.0),
            child:
            // Adobe XD layer: 'Group 4' (group)
            InkWell(
              onTap: (){
                print("on tap called ${quatityCount}");

                  if(quatityCount>2){
                    quatityCount=quatityCount-1;
                  }
              },
              child: Stack(
                children: <Widget>[
                  // Adobe XD layer: 'Oval' (shape)
                  Container(
                    width: 44.0,
                    height: 44.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.elliptical(22.0, 22.0)),
                      color: const Color(0xffffffff),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(14.0, 21.0),
                    child:
                    // Adobe XD layer: 'Plus Copy' (group)
                    Stack(
                      children: <Widget>[
                        // Adobe XD layer: 'Rectangle-185' (shape)
                        Container(
                          width: 16.0,
                          height: 2.0,
                          decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.circular(1.0),
                            color: const Color(0xff365eff),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(267.0, -13.0),
            child:
            // Adobe XD layer: 'Group 3' (group)
            InkWell(
              onTap: (){

                  quatityCount=quatityCount+1;


              },
              child: Stack(
                children: <Widget>[
                  // Adobe XD layer: 'Oval Copy' (shape)
                  Container(
                    width: 44.0,
                    height: 44.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.elliptical(22.0, 22.0)),
                      color: const Color(0xffffffff),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(14.0, 14.0),
                    child:
                    // Adobe XD layer: 'Plus' (group)
                    Stack(
                      children: <Widget>[
                        Transform.translate(
                          offset: Offset(0.0, 7.0),
                          child:
                          // Adobe XD layer: 'Rectangle-185' (shape)
                          Container(
                            width: 16.0,
                            height: 2.0,
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.circular(1.0),
                              color: const Color(0xff365eff),
                            ),
                          ),
                        ),
                        Transform(
                          transform: Matrix4(
                              0.0,
                              1.0,
                              0.0,
                              0.0,
                              -1.0,
                              0.0,
                              0.0,
                              0.0,
                              0.0,
                              0.0,
                              1.0,
                              0.0,
                              9.0,
                              0.0,
                              0.0,
                              1.0),
                          child:
                          // Adobe XD layer: 'Rectangle-185-Copy' (shape)
                          Container(
                            width: 16.0,
                            height: 2.0,
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.circular(1.0),
                              color: const Color(0xff365eff),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, -2.0),
            child:
            // Adobe XD layer: 'Quality' (text)
            Text(
              AppLocalizations.of(context).commonLocalization.appLocalizations_Labels_Common_Sub_Titles.quantitySubTitle,
              style: TextStyle(
                fontFamily: 'fonts/Montserrat-Medium',
                fontSize: 18,
                color: const Color(0xff656565),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(235.0, -2.0),
            child:
            // Adobe XD layer: '1' (text)
            Text(
              '${quatityCount}',
              style: TextStyle(
                fontFamily: 'fonts/Montserrat-Bold',
                fontSize: 18,
                color: const Color(0xff656565),
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}
