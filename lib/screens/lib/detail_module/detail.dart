import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iamsng/localization/app_localization/app_localization.dart';
import 'package:iamsng/model/restaurent_response_model.dart';
import 'package:iamsng/screens/lib/detail_module/quantiry_view.dart';

class Detail extends StatefulWidget {
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {

  Restaurant restaurant;

  @override
  Widget build(BuildContext context) {
    return _getMainView(context);
  }


  _getMainView(BuildContext context) {
    restaurant = ModalRoute
        .of(context)
        .settings
        .arguments;

    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(0.5, 0.0),
                  child: Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height, //812.0,
                    decoration: BoxDecoration(
                      color: const Color(0xffffffff),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(0.0, 44.0),
                  child:
                  // Adobe XD layer: 'Rectangle' (shape)
                  Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height / 1.3, //768.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(32.0),
                      color: const Color(0x6ef9f8f7),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(0.0, 60.0),
                  child:
                  // Adobe XD layer: 'Group 5' (group)
                  Stack(
                    children: <Widget>[
                      Transform.translate(
                        offset: Offset(0.0, MediaQuery
                            .of(context)
                            .size
                            .height / 1.6),
                        child:
                        // Adobe XD layer: 'Add to card' (group)
                        Stack(
                          children: <Widget>[
                            // Adobe XD layer: 'Rectangle Copy' (shape)
                            Container(
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width,
                              height: 220.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(44.0),
                                ),
                                color: const Color(0xff365eff),
                                boxShadow: [
                                  BoxShadow(
                                    color: const Color(0x1a000000),
                                    offset: Offset(8, 16),
                                    blurRadius: 32,
                                  ),
                                ],
                              ),
                            ),
                            Transform.translate(
                              offset: Offset(MediaQuery
                                  .of(context)
                                  .size
                                  .width / 2.5, MediaQuery
                                  .of(context)
                                  .size
                                  .height / 5),
                              child:
                              // Adobe XD layer: 'Add to card' (text)
                              Text(
                                AppLocalizations.of(context).commonButtonText.assToCart,
                                style: TextStyle(
                                  fontFamily: 'fonts/Montserrat-Bold',
                                  fontSize: 18,
                                  color: const Color(0xffffffff),
                                  height: 1.3333333333333333,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(0.0, 163.0),
                        child:
                        // Adobe XD layer: 'Rectangle Copy' (shape)
                        Container(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          height: MediaQuery
                              .of(context)
                              .size
                              .height / 1.83, //489.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(44.0),
                              bottomRight: Radius.circular(11.0),
                              bottomLeft: Radius.circular(44.0),
                            ),
                            color: const Color(0xffedf0ff),
                          ),
                        ),
                      ),

                      QuantityView(),

                      // Adobe XD layer: 'Rectangle' (shape)
                      Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: MediaQuery
                            .of(context)
                            .size
                            .height / 1.5,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(44.0),
                            topRight: Radius.circular(44.0),
                            bottomRight: Radius.circular(11.0),
                            bottomLeft: Radius.circular(44.0),
                          ),
                          color: const Color(0xfffcf9f7),
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0x1a000000),
                              offset: Offset(8, -16),
                              blurRadius: 32,
                            ),
                          ],
                        ),
                      ),

                      //description view
                      Transform.translate(
                        offset: Offset(32.0, MediaQuery
                            .of(context)
                            .size
                            .height / 1.82),
                        child:
                        // Adobe XD layer: 'Fresh hamburger with' (text)
                        SizedBox(
                          width: MediaQuery.of(context).size.width-50.0,
                          height: 54.0,
                          child: Text(
                            '${restaurant.userRating.aggregateRating}  ||  ${restaurant.userRating.votes} Votes\n${restaurant.location.address}',
                            style: TextStyle(
                              fontFamily: 'fonts/Montserrat-Regular',
                              fontSize: 12,
                              color: const Color(0xff656565),
                              height: 1.5,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),

                      //$30
                      Transform.translate(
                        offset: Offset(32.0, MediaQuery
                            .of(context)
                            .size
                            .height / 2.2 /*373.0*/),
                        child:
                        // Adobe XD layer: '$30.00' (text)
                        Text(
                          '${restaurant.currency} ${restaurant.averageCostForTwo}',
                          style: TextStyle(
                            fontFamily: 'fonts/Montserrat-SemiBold',
                            fontSize: 24,
                            color: const Color(0xff365eff),
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),

                      //chicken hamberger view
                      Transform.translate(
                        offset: Offset(32.0, MediaQuery
                            .of(context)
                            .size
                            .height / 2.03),
                        child:
                        // Adobe XD layer: 'Chicken Hamburger' (text)
                        Text(
                          restaurant.name,
                          style: TextStyle(
                            fontFamily: 'fonts/Roboto',
                            fontSize: 24,
                            color: const Color(0xff373737),
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),


                      Transform.translate(
                        offset: Offset(MediaQuery
                            .of(context)
                            .size
                            .width / 4.3 /*65.0*/, MediaQuery
                            .of(context)
                            .size
                            .height / 9.5 /*80.0*/),
                        child:Stack(
                        children: <Widget>[
                          Container(
                        width: MediaQuery
                                  .of(context)
                                  .size
                                  .height / 3.4,
                              height: MediaQuery
                                  .of(context)
                                  .size
                                  .height / 3.4,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.elliptical(122.5, 122.5)),
                              color: const Color(0xffd8d8d8),
                            ),
                          ),
                          Transform.translate(
                            //offset: Offset(-1.01, -5.23),
                            offset: Offset(0.0, 0.0),
                            child:
                            // Adobe XD layer: 'twenty20_3998e4bc-f…' (shape)
                            CachedNetworkImage(
                              imageUrl: restaurant.featuredImage,
                              placeholder: (context, url) => const CircleAvatar(
                                  backgroundColor: const Color(0xffd8d8d8),
                                  radius: 105,
                                  child:CircularProgressIndicator()
                              ),
                              imageBuilder: (context, image) => CircleAvatar(
                                radius: 105,
                                backgroundColor: const Color(0xffd8d8d8),
                                child: CircleAvatar(
                                  radius: 102,
                                  backgroundImage: image,
                                ),
                              ),
                            ),
                          ),
                          // Adobe XD layer: 'Mask' (shape)

                        ],
                      ),),

                      //video play button
                      Transform.translate(
                        offset: Offset(152.0, MediaQuery
                            .of(context)
                            .size
                            .height / 4.6 /*179.0*/),
                        child:
                        // Adobe XD layer: 'Btn Play' (group)
                        Stack(
                          children: <Widget>[
                            // Adobe XD layer: 'Rectangle' (shape)
                            Container(
                              width: 72.0,
                              height: 48.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(100.0),
                                  topRight: Radius.circular(32.0),
                                  bottomRight: Radius.circular(100.0),
                                  bottomLeft: Radius.circular(32.0),
                                ),
                                color: const Color(0x99373737),
                              ),
                            ),
                            Transform.translate(
                              offset: Offset(24.0, 12.0),
                              child:
                              // Adobe XD layer: 'Play' (group)
                              Stack(
                                children: <Widget>[
                                  // Adobe XD layer: 'bound' (shape)
                                  SvgPicture.string(
                                    _svg_khr0j7,
                                    allowDrawingOutsideViewBox: true,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      //top right heart
                      Transform.translate(
                        offset: Offset(MediaQuery
                            .of(context)
                            .size
                            .width / 1.28, 16.0),
                        child:
                        // Adobe XD layer: 'Btn heart' (group)
                        Stack(
                          children: <Widget>[
                            // Adobe XD layer: 'Rectangle' (shape)
                            Container(
                              width: 72.0,
                              height: 48.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(32.0),
                                  bottomLeft: Radius.circular(32.0),
                                ),
                                color: const Color(0xff99adff),
                              ),
                            ),
                            Transform.translate(
                              offset: Offset(24.0, 12.0),
                              child:
                              // Adobe XD layer: 'Heart' (group)
                              Stack(
                                children: <Widget>[
                                  // Adobe XD layer: 'Shape' (shape)
                                  SvgPicture.string(
                                    (restaurant.isFav)?_svg_qqrib0:_svg_q5rs9t,
                                    allowDrawingOutsideViewBox: true,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      //center horizontal line
                      Transform.translate(
                        offset: Offset(176.0, 8.0),
                        child:
                        // Adobe XD layer: 'Rectangle' (shape)
                        Container(
                          width: 24.0,
                          height: 4.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2.5),
                            color: const Color(0xff365eff),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}




const String _svg_khr0j7 =
    '<svg viewBox="0.0 0.0 24.0 24.0" ><path  d="M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /><path transform="translate(7.87, 4.0)" d="M 9.778535842895508 8.493793487548828 C 9.918789863586426 8.375452041625977 9.999500274658203 8.204324722290039 9.999995231628418 8.024245262145996 C 10.00049018859863 7.844165325164795 9.920721054077148 7.672625541687012 9.781119346618652 7.553563594818115 L 1.072293400764465 0.1555073708295822 C 0.8816385865211487 -0.006591226439923048 0.6105725169181824 -0.045630794018507 0.3794079720973969 0.05571639910340309 C 0.1482434570789337 0.1570635885000229 -0.0001858084578998387 0.3800183832645416 6.267201229093189e-07 0.6256221532821655 L 6.267201229093189e-07 15.37422275543213 C -0.0002484316355548799 15.619553565979 0.1477976590394974 15.8423490524292 0.3785530030727386 15.94390869140625 C 0.6093083024024963 16.04546737670898 0.8801144361495972 16.00701522827148 1.071001529693604 15.84558868408203 L 9.778535842895508 8.493793487548828 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_qqrib0 =
    '<svg viewBox="0.0 0.0 24.0 24.0" ><path  d="M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /><path transform="translate(3.0, 4.5)" d="M 13.5 0 C 11.89000034332275 0 10.00800037384033 1.825000047683716 9 3 C 7.992000102996826 1.825000047683716 6.110000133514404 0 4.5 0 C 1.651000022888184 0 0 2.221999883651733 0 5.050000190734863 C 0 8.182999610900879 3 11.5 9 15 C 15 11.5 18 8.25 18 5.25 C 18 2.421999931335449 16.34900093078613 0 13.5 0 Z" fill="#365eff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
//white heart
const String _svg_q5rs9t =
    '<svg viewBox="0.0 0.0 24.0 24.0" ><path  d="M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /><path transform="translate(3.0, 4.5)" d="M 13.5 0 C 11.89000034332275 0 10.00800037384033 1.825000047683716 9 3 C 7.992000102996826 1.825000047683716 6.110000133514404 0 4.5 0 C 1.651000022888184 0 0 2.221999883651733 0 5.050000190734863 C 0 8.182999610900879 3 11.5 9 15 C 15 11.5 18 8.25 18 5.25 C 18 2.421999931335449 16.34900093078613 0 13.5 0 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
