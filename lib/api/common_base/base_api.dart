

import 'package:dio/dio.dart';
import 'package:iamsng/common/constants/app_constants.dart';


class BaseApi {
  static final BaseApi _BaseApi = BaseApi._internal();
  Dio _dio;

  static BaseApi getInstance() {
    return _BaseApi;
  }

  factory BaseApi() => _BaseApi;

  BaseApi._internal() {
    BaseOptions baseOptions = BaseOptions(
      baseUrl: BASE_URL,
      connectTimeout: 10000,
      receiveTimeout: 10000,
    );
    _dio = Dio(baseOptions);

    _dio.interceptors.add(LogInterceptor(requestBody: true, responseBody: true, error: true));
  }

  Dio getClient() {

    _dio.options.headers["Accept"] = 'application/json';
    _dio.options.headers["user-key"] = USER_KEY;

    return _dio;
  }

}

