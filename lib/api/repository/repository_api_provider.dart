
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:iamsng/api/common_base/base_api.dart';
import 'package:iamsng/common/constants/app_constants.dart';
import 'package:iamsng/common/utils/app_utils.dart';
import 'package:iamsng/model/restaurent_response_model.dart';
import 'package:iamsng/state/repository_state.dart';


class RepositoryApiProvider {
  static RepositoryApiProvider _apiProvider = new RepositoryApiProvider.internal();

  Dio _dio = BaseApi.getInstance().getClient();

  RepositoryApiProvider.internal();

  factory RepositoryApiProvider() => _apiProvider;


  Future<RepositoryState> getCall(String queryParam) async {

    try {
      var result = await _dio.get(queryParam);

    //  print("status ${result.statusCode}");
      RepositoryState repositoryState= RepositoryState();


      if(result.statusCode==200 ) {

        //AppUtils.printWrapped(result.data);

        repositoryState.responseModel =
            RestaurantsResponseModel.fromJson(result.data);

        AppUtils.printWrapped(json.encode(repositoryState.responseModel.toJson()));
        repositoryState.status = SUCCESS;

        return repositoryState;


      }else{

        repositoryState.status=FAILURE;

        return repositoryState;
      }

    } on DioError catch (e) {

        // Something happened in setting up or sending the request that triggered an Error
          RepositoryState repositoryState= RepositoryState();

          repositoryState.status=FAILURE;

          return repositoryState;

    }
  }


}