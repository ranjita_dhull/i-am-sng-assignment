import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:iamsng/custom_widgets/default_alert_dialog.dart';
import 'package:iamsng/localization/app_localization/app_localization.dart';


class NetworkCheck {
  NetworkCheck.internal();

  static NetworkCheck instance = new NetworkCheck.internal();

  factory NetworkCheck() => instance;

  Future<bool> getConnectivityStatus(BuildContext context, {bool showSnackBar = true}) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
// I am connected to a mobile network.
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
// I am connected to a wifi network.
      return true;
    } else {
      if (showSnackBar) {
        InfoAlertDialog.infoAlert(context, AppLocalizations.of(context).commonLocalization.appLocalizations_Labels_Common_Titles.noInternetConnectionTitle, AppLocalizations.of(context).commonLocalization.appLocalizations_Labels_Common_Titles.popularTitle);
      }
      return false;
    }
  }
}
