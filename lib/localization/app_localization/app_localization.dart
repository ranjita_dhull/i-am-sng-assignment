import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import '../common_button_localization.dart';
import '../common_title_localization.dart';


class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) =>
      SynchronousFuture<AppLocalizations>(AppLocalizations(locale));

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}

class AppLocalizations {
  AppLocalizations(this.locale) : this.labels = languages[locale];

  final Locale locale;

  static final Map<Locale, AppLocalizations_Labels> languages = {
    Locale.fromSubtags(languageCode: "en"): AppLocalizations_Labels(



      /*---------Common Titles Start-------------*/

      commonLocalization: AppLocalizations_Common(
          appLocalizations_Labels_Common_Titles:
              AppLocalizations_Labels_Common_Titles(
                  foodAndDeliveryTitle: 'Food & Delivery',
                  somethingWentWrongTitle: 'Something Went Wrong..',
                  nearYouTitle: 'Near You',
                noInternetConnectionTitle: "No internt Connection.",
                popularTitle: "Popular",
                usernameOrPasswordIncorrectTitle: "Username / Password is incorrect."
                  ),

          appLocalizations_Labels_Common_Sub_Titles:
              AppLocalizations_Labels_Common_Sub_Titles(
                  myProfileSubTitle: "My Profile.",
              invoiceSubTitle: "Invoice",
              notificationSubTitle: "Notification",
              homeSubTitle: "Home",
              quantitySubTitle: "Quantity"),


      ),


      /*---------Common Titles Start-------------*/

      /*---------Common Button Text Start-------------*/

      commonButtonText: AppLocalizations_Labels_Common_Button_Text(
          viewAll: 'View All',
        assToCart: 'Add to card',
          ),

      /*---------Common Button Text Ends-------------*/


    )
  };
  final AppLocalizations_Labels labels;

  static AppLocalizations_Labels of(BuildContext context) =>
      Localizations.of<AppLocalizations>(context, AppLocalizations)?.labels;
}

class AppLocalizations_Labels {
  const AppLocalizations_Labels(
      {
      this.commonButtonText,
      this.commonLocalization,
   });


  final AppLocalizations_Labels_Common_Button_Text commonButtonText;
  final AppLocalizations_Common commonLocalization;

}
