//This is a common text class

class AppLocalizations_Common {
  AppLocalizations_Common(
      {this.appLocalizations_Labels_Common_Titles,
      this.appLocalizations_Labels_Common_Sub_Titles,
     }) {}

  AppLocalizations_Labels_Common_Titles appLocalizations_Labels_Common_Titles;

  AppLocalizations_Labels_Common_Sub_Titles
      appLocalizations_Labels_Common_Sub_Titles;


}

class AppLocalizations_Labels_Common_Titles {
  AppLocalizations_Labels_Common_Titles(
      {this.foodAndDeliveryTitle,
      this.somethingWentWrongTitle,
        this.nearYouTitle,
        this.noInternetConnectionTitle,
        this.popularTitle,
        this.usernameOrPasswordIncorrectTitle
      });

  final String foodAndDeliveryTitle;
  final String somethingWentWrongTitle;
  final String noInternetConnectionTitle;
  final String nearYouTitle;
  final String popularTitle;
  final String usernameOrPasswordIncorrectTitle;

}

class AppLocalizations_Labels_Common_Sub_Titles {
  AppLocalizations_Labels_Common_Sub_Titles({this.myProfileSubTitle,
  this.invoiceSubTitle,this.notificationSubTitle, this.homeSubTitle, this.quantitySubTitle});

  final String myProfileSubTitle;
  final String notificationSubTitle;
  final String invoiceSubTitle;
  final String homeSubTitle;
  final String quantitySubTitle;
}


