// To parse this JSON data, do
//
//     final restaurantsResponseModel = restaurantsResponseModelFromJson(jsonString);

import 'dart:convert';

RestaurantsResponseModel restaurantsResponseModelFromJson(String str) => RestaurantsResponseModel.fromJson(json.decode(str));

String restaurantsResponseModelToJson(RestaurantsResponseModel data) => json.encode(data.toJson());

class RestaurantsResponseModel {
  RestaurantsResponseModel({
    this.location,
    this.popularity,
    this.link,
    this.nearbyRestaurants,
  });

  RestaurantsResponseModelLocation location;
  Popularity popularity;
  String link;
  List<NearbyRestaurant> nearbyRestaurants;


  factory RestaurantsResponseModel.fromJson(Map<String, dynamic> json) => RestaurantsResponseModel(
    location: json["location"] == null ? null : RestaurantsResponseModelLocation.fromJson(json["location"]),
    popularity: json["popularity"] == null ? null : Popularity.fromJson(json["popularity"]),
    link: json["link"] == null ? null : json["link"],
    nearbyRestaurants: json["nearby_restaurants"] == null ? null : List<NearbyRestaurant>.from(json["nearby_restaurants"].map((x) => NearbyRestaurant.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "location": location == null ? null : location.toJson(),
    "popularity": popularity == null ? null : popularity.toJson(),
    "link": link == null ? null : link,
    "nearby_restaurants": nearbyRestaurants == null ? null : List<dynamic>.from(nearbyRestaurants.map((x) => x.toJson())),
  };
}

class CuisineWiseClassification{
  List<NearbyRestaurant> nearbyRestaurants=new List<NearbyRestaurant>();
  List<NearbyRestaurant> popularRestaurants= new List<NearbyRestaurant>();
}

class RestaurantsResponseModelLocation {
  RestaurantsResponseModelLocation({
    this.entityType,
    this.entityId,
    this.title,
    this.latitude,
    this.longitude,
    this.cityId,
    this.cityName,
    this.countryId,
    this.countryName,
  });

  String entityType;
  int entityId;
  String title;
  String latitude;
  String longitude;
  int cityId;
  String cityName;
  int countryId;
  String countryName;

  factory RestaurantsResponseModelLocation.fromJson(Map<String, dynamic> json) => RestaurantsResponseModelLocation(
    entityType: json["entity_type"] == null ? null : json["entity_type"],
    entityId: json["entity_id"] == null ? null : json["entity_id"],
    title: json["title"] == null ? null : json["title"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
    cityId: json["city_id"] == null ? null : json["city_id"],
    cityName: json["city_name"] == null ? null : json["city_name"],
    countryId: json["country_id"] == null ? null : json["country_id"],
    countryName: json["country_name"] == null ? null : json["country_name"],
  );

  Map<String, dynamic> toJson() => {
    "entity_type": entityType == null ? null : entityType,
    "entity_id": entityId == null ? null : entityId,
    "title": title == null ? null : title,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "city_id": cityId == null ? null : cityId,
    "city_name": cityName == null ? null : cityName,
    "country_id": countryId == null ? null : countryId,
    "country_name": countryName == null ? null : countryName,
  };
}

class NearbyRestaurant {
  NearbyRestaurant({
    this.restaurant,
  });

  Restaurant restaurant;

  factory NearbyRestaurant.fromJson(Map<String, dynamic> json) => NearbyRestaurant(
    restaurant: json["restaurant"] == null ? null : Restaurant.fromJson(json["restaurant"]),
  );

  Map<String, dynamic> toJson() => {
    "restaurant": restaurant == null ? null : restaurant.toJson(),
  };
}

class Restaurant {
  Restaurant({
    this.r,
    this.apikey,
    this.id,
    this.name,
    this.url,
    this.location,
    this.switchToOrderMenu,
    this.cuisines,
    this.averageCostForTwo,
    this.priceRange,
    this.currency,
    this.offers,
    this.opentableSupport,
    this.isZomatoBookRes,
    this.mezzoProvider,
    this.isBookFormWebView,
    this.bookFormWebViewUrl,
    this.bookAgainUrl,
    this.thumb,
    this.userRating,
    this.photosUrl,
    this.menuUrl,
    this.featuredImage,
    this.hasOnlineDelivery,
    this.isDeliveringNow,
    this.storeType,
    this.includeBogoOffers,
    this.deeplink,
    this.isTableReservationSupported,
    this.hasTableBooking,
    this.eventsUrl,
    this.orderUrl,
    this.orderDeeplink,
    this.zomatoEvents,
  });

  R r;
  String apikey;
  String id;
  String name;
  String url;
  RestaurantLocation location;
  int switchToOrderMenu;
  String cuisines;
  int averageCostForTwo;
  int priceRange;
  String currency;
  List<dynamic> offers;
  int opentableSupport;
  int isZomatoBookRes;
  String mezzoProvider;
  int isBookFormWebView;
  String bookFormWebViewUrl;
  String bookAgainUrl;
  String thumb;
  UserRating userRating;
  String photosUrl;
  String menuUrl;
  String featuredImage;
  int hasOnlineDelivery;
  int isDeliveringNow;
  String storeType;
  bool includeBogoOffers;
  String deeplink;
  int isTableReservationSupported;
  int hasTableBooking;
  String eventsUrl;
  String orderUrl;
  String orderDeeplink;
  List<ZomatoEvent> zomatoEvents;
  bool isFav=false;

  factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
    r: json["R"] == null ? null : R.fromJson(json["R"]),
    apikey: json["apikey"] == null ? null : json["apikey"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    url: json["url"] == null ? null : json["url"],
    location: json["location"] == null ? null : RestaurantLocation.fromJson(json["location"]),
    switchToOrderMenu: json["switch_to_order_menu"] == null ? null : json["switch_to_order_menu"],
    cuisines: json["cuisines"] == null ? null : json["cuisines"],
    averageCostForTwo: json["average_cost_for_two"] == null ? null : json["average_cost_for_two"],
    priceRange: json["price_range"] == null ? null : json["price_range"],
    currency: json["currency"] == null ? null : json["currency"],
    offers: json["offers"] == null ? null : List<dynamic>.from(json["offers"].map((x) => x)),
    opentableSupport: json["opentable_support"] == null ? null : json["opentable_support"],
    isZomatoBookRes: json["is_zomato_book_res"] == null ? null : json["is_zomato_book_res"],
    mezzoProvider: json["mezzo_provider"] == null ? null : json["mezzo_provider"],
    isBookFormWebView: json["is_book_form_web_view"] == null ? null : json["is_book_form_web_view"],
    bookFormWebViewUrl: json["book_form_web_view_url"] == null ? null : json["book_form_web_view_url"],
    bookAgainUrl: json["book_again_url"] == null ? null : json["book_again_url"],
    thumb: json["thumb"] == null ? null : json["thumb"],
    userRating: json["user_rating"] == null ? null : UserRating.fromJson(json["user_rating"]),
    photosUrl: json["photos_url"] == null ? null : json["photos_url"],
    menuUrl: json["menu_url"] == null ? null : json["menu_url"],
    featuredImage: json["featured_image"] == null ? null : json["featured_image"],
    hasOnlineDelivery: json["has_online_delivery"] == null ? null : json["has_online_delivery"],
    isDeliveringNow: json["is_delivering_now"] == null ? null : json["is_delivering_now"],
    storeType: json["store_type"] == null ? null : json["store_type"],
    includeBogoOffers: json["include_bogo_offers"] == null ? null : json["include_bogo_offers"],
    deeplink: json["deeplink"] == null ? null : json["deeplink"],
    isTableReservationSupported: json["is_table_reservation_supported"] == null ? null : json["is_table_reservation_supported"],
    hasTableBooking: json["has_table_booking"] == null ? null : json["has_table_booking"],
    eventsUrl: json["events_url"] == null ? null : json["events_url"],
    orderUrl: json["order_url"] == null ? null : json["order_url"],
    orderDeeplink: json["order_deeplink"] == null ? null : json["order_deeplink"],
    zomatoEvents: json["zomato_events"] == null ? null : List<ZomatoEvent>.from(json["zomato_events"].map((x) => ZomatoEvent.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "R": r == null ? null : r.toJson(),
    "apikey": apikey == null ? null : apikey,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "url": url == null ? null : url,
    "location": location == null ? null : location.toJson(),
    "switch_to_order_menu": switchToOrderMenu == null ? null : switchToOrderMenu,
    "cuisines": cuisines == null ? null : cuisines,
    "average_cost_for_two": averageCostForTwo == null ? null : averageCostForTwo,
    "price_range": priceRange == null ? null : priceRange,
    "currency": currency == null ? null : currency,
    "offers": offers == null ? null : List<dynamic>.from(offers.map((x) => x)),
    "opentable_support": opentableSupport == null ? null : opentableSupport,
    "is_zomato_book_res": isZomatoBookRes == null ? null : isZomatoBookRes,
    "mezzo_provider": mezzoProvider == null ? null : mezzoProvider,
    "is_book_form_web_view": isBookFormWebView == null ? null : isBookFormWebView,
    "book_form_web_view_url": bookFormWebViewUrl == null ? null : bookFormWebViewUrl,
    "book_again_url": bookAgainUrl == null ? null : bookAgainUrl,
    "thumb": thumb == null ? null : thumb,
    "user_rating": userRating == null ? null : userRating.toJson(),
    "photos_url": photosUrl == null ? null : photosUrl,
    "menu_url": menuUrl == null ? null : menuUrl,
    "featured_image": featuredImage == null ? null : featuredImage,
    "has_online_delivery": hasOnlineDelivery == null ? null : hasOnlineDelivery,
    "is_delivering_now": isDeliveringNow == null ? null : isDeliveringNow,
    "store_type": storeType == null ? null : storeType,
    "include_bogo_offers": includeBogoOffers == null ? null : includeBogoOffers,
    "deeplink": deeplink == null ? null : deeplink,
    "is_table_reservation_supported": isTableReservationSupported == null ? null : isTableReservationSupported,
    "has_table_booking": hasTableBooking == null ? null : hasTableBooking,
    "events_url": eventsUrl == null ? null : eventsUrl,
    "order_url": orderUrl == null ? null : orderUrl,
    "order_deeplink": orderDeeplink == null ? null : orderDeeplink,
    "zomato_events": zomatoEvents == null ? null : List<dynamic>.from(zomatoEvents.map((x) => x.toJson())),
  };
}

class RestaurantLocation {
  RestaurantLocation({
    this.address,
    this.locality,
    this.city,
    this.cityId,
    this.latitude,
    this.longitude,
    this.zipcode,
    this.countryId,
    this.localityVerbose,
  });

  String address;
  String locality;
  String city;
  int cityId;
  String latitude;
  String longitude;
  String zipcode;
  int countryId;
  String localityVerbose;

  factory RestaurantLocation.fromJson(Map<String, dynamic> json) => RestaurantLocation(
    address: json["address"] == null ? null : json["address"],
    locality: json["locality"] == null ? null : json["locality"],
    city: json["city"] == null ? null : json["city"],
    cityId: json["city_id"] == null ? null : json["city_id"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
    zipcode: json["zipcode"] == null ? null : json["zipcode"],
    countryId: json["country_id"] == null ? null : json["country_id"],
    localityVerbose: json["locality_verbose"] == null ? null : json["locality_verbose"],
  );

  Map<String, dynamic> toJson() => {
    "address": address == null ? null : address,
    "locality": locality == null ? null : locality,
    "city": city == null ? null : city,
    "city_id": cityId == null ? null : cityId,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "zipcode": zipcode == null ? null : zipcode,
    "country_id": countryId == null ? null : countryId,
    "locality_verbose": localityVerbose == null ? null : localityVerbose,
  };
}

class R {
  R({
    this.hasMenuStatus,
    this.resId,
    this.isGroceryStore,
  });

  HasMenuStatus hasMenuStatus;
  int resId;
  bool isGroceryStore;

  factory R.fromJson(Map<String, dynamic> json) => R(
    hasMenuStatus: json["has_menu_status"] == null ? null : HasMenuStatus.fromJson(json["has_menu_status"]),
    resId: json["res_id"] == null ? null : json["res_id"],
    isGroceryStore: json["is_grocery_store"] == null ? null : json["is_grocery_store"],
  );

  Map<String, dynamic> toJson() => {
    "has_menu_status": hasMenuStatus == null ? null : hasMenuStatus.toJson(),
    "res_id": resId == null ? null : resId,
    "is_grocery_store": isGroceryStore == null ? null : isGroceryStore,
  };
}

class HasMenuStatus {
  HasMenuStatus({
    this.delivery,
    this.takeaway,
  });

  int delivery;
  int takeaway;

  factory HasMenuStatus.fromJson(Map<String, dynamic> json) => HasMenuStatus(
    delivery: json["delivery"] == null ? null : json["delivery"],
    takeaway: json["takeaway"] == null ? null : json["takeaway"],
  );

  Map<String, dynamic> toJson() => {
    "delivery": delivery == null ? null : delivery,
    "takeaway": takeaway == null ? null : takeaway,
  };
}

class UserRating {
  UserRating({
    this.aggregateRating,
    this.ratingText,
    this.ratingColor,
    this.ratingObj,
    this.votes,
  });

  String aggregateRating;
  String ratingText;
  String ratingColor;
  RatingObj ratingObj;
  String votes;

  factory UserRating.fromJson(Map<String, dynamic> json) => UserRating(
    aggregateRating: json["aggregate_rating"] == null ? null : json["aggregate_rating"],
    ratingText: json["rating_text"] == null ? null : json["rating_text"],
    ratingColor: json["rating_color"] == null ? null : json["rating_color"],
    ratingObj: json["rating_obj"] == null ? null : RatingObj.fromJson(json["rating_obj"]),
    votes: json["votes"] == null ? null : json["votes"],
  );

  Map<String, dynamic> toJson() => {
    "aggregate_rating": aggregateRating == null ? null : aggregateRating,
    "rating_text": ratingText == null ? null : ratingText,
    "rating_color": ratingColor == null ? null : ratingColor,
    "rating_obj": ratingObj == null ? null : ratingObj.toJson(),
    "votes": votes == null ? null : votes,
  };
}

class RatingObj {
  RatingObj({
    this.title,
    this.bgColor,
  });

  Title title;
  BgColor bgColor;

  factory RatingObj.fromJson(Map<String, dynamic> json) => RatingObj(
    title: json["title"] == null ? null : Title.fromJson(json["title"]),
    bgColor: json["bg_color"] == null ? null : BgColor.fromJson(json["bg_color"]),
  );

  Map<String, dynamic> toJson() => {
    "title": title == null ? null : title.toJson(),
    "bg_color": bgColor == null ? null : bgColor.toJson(),
  };
}

class BgColor {
  BgColor({
    this.type,
    this.tint,
  });

  String type;
  String tint;

  factory BgColor.fromJson(Map<String, dynamic> json) => BgColor(
    type: json["type"] == null ? null : json["type"],
    tint: json["tint"] == null ? null : json["tint"],
  );

  Map<String, dynamic> toJson() => {
    "type": type == null ? null : type,
    "tint": tint == null ? null : tint,
  };
}

class Title {
  Title({
    this.text,
  });

  String text;

  factory Title.fromJson(Map<String, dynamic> json) => Title(
    text: json["text"] == null ? null : json["text"],
  );

  Map<String, dynamic> toJson() => {
    "text": text == null ? null : text,
  };
}

class ZomatoEvent {
  ZomatoEvent({
    this.event,
  });

  Event event;

  factory ZomatoEvent.fromJson(Map<String, dynamic> json) => ZomatoEvent(
    event: json["event"] == null ? null : Event.fromJson(json["event"]),
  );

  Map<String, dynamic> toJson() => {
    "event": event == null ? null : event.toJson(),
  };
}

class Event {
  Event({
    this.eventId,
    this.friendlyStartDate,
    this.friendlyEndDate,
    this.friendlyTimingStr,
    this.startDate,
    this.endDate,
    this.endTime,
    this.startTime,
    this.isActive,
    this.dateAdded,
    this.photos,
    this.restaurants,
    this.isValid,
    this.shareUrl,
    this.showShareUrl,
    this.title,
    this.description,
    this.displayTime,
    this.displayDate,
    this.isEndTimeSet,
    this.disclaimer,
    this.eventCategory,
    this.eventCategoryName,
    this.bookLink,
    this.types,
    this.shareData,
  });

  int eventId;
  String friendlyStartDate;
  String friendlyEndDate;
  String friendlyTimingStr;
  DateTime startDate;
  DateTime endDate;
  String endTime;
  String startTime;
  int isActive;
  DateTime dateAdded;
  List<PhotoElement> photos;
  List<dynamic> restaurants;
  int isValid;
  String shareUrl;
  int showShareUrl;
  String title;
  String description;
  String displayTime;
  String displayDate;
  int isEndTimeSet;
  String disclaimer;
  int eventCategory;
  String eventCategoryName;
  String bookLink;
  List<dynamic> types;
  ShareData shareData;

  factory Event.fromJson(Map<String, dynamic> json) => Event(
    eventId: json["event_id"] == null ? null : json["event_id"],
    friendlyStartDate: json["friendly_start_date"] == null ? null : json["friendly_start_date"],
    friendlyEndDate: json["friendly_end_date"] == null ? null : json["friendly_end_date"],
    friendlyTimingStr: json["friendly_timing_str"] == null ? null : json["friendly_timing_str"],
    startDate: json["start_date"] == null ? null : DateTime.parse(json["start_date"]),
    endDate: json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
    endTime: json["end_time"] == null ? null : json["end_time"],
    startTime: json["start_time"] == null ? null : json["start_time"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    dateAdded: json["date_added"] == null ? null : DateTime.parse(json["date_added"]),
    photos: json["photos"] == null ? null : List<PhotoElement>.from(json["photos"].map((x) => PhotoElement.fromJson(x))),
    restaurants: json["restaurants"] == null ? null : List<dynamic>.from(json["restaurants"].map((x) => x)),
    isValid: json["is_valid"] == null ? null : json["is_valid"],
    shareUrl: json["share_url"] == null ? null : json["share_url"],
    showShareUrl: json["show_share_url"] == null ? null : json["show_share_url"],
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    displayTime: json["display_time"] == null ? null : json["display_time"],
    displayDate: json["display_date"] == null ? null : json["display_date"],
    isEndTimeSet: json["is_end_time_set"] == null ? null : json["is_end_time_set"],
    disclaimer: json["disclaimer"] == null ? null : json["disclaimer"],
    eventCategory: json["event_category"] == null ? null : json["event_category"],
    eventCategoryName: json["event_category_name"] == null ? null : json["event_category_name"],
    bookLink: json["book_link"] == null ? null : json["book_link"],
    types: json["types"] == null ? null : List<dynamic>.from(json["types"].map((x) => x)),
    shareData: json["share_data"] == null ? null : ShareData.fromJson(json["share_data"]),
  );

  Map<String, dynamic> toJson() => {
    "event_id": eventId == null ? null : eventId,
    "friendly_start_date": friendlyStartDate == null ? null : friendlyStartDate,
    "friendly_end_date": friendlyEndDate == null ? null : friendlyEndDate,
    "friendly_timing_str": friendlyTimingStr == null ? null : friendlyTimingStr,
    "start_date": startDate == null ? null : "${startDate.year.toString().padLeft(4, '0')}-${startDate.month.toString().padLeft(2, '0')}-${startDate.day.toString().padLeft(2, '0')}",
    "end_date": endDate == null ? null : "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
    "end_time": endTime == null ? null : endTime,
    "start_time": startTime == null ? null : startTime,
    "is_active": isActive == null ? null : isActive,
    "date_added": dateAdded == null ? null : dateAdded.toIso8601String(),
    "photos": photos == null ? null : List<dynamic>.from(photos.map((x) => x.toJson())),
    "restaurants": restaurants == null ? null : List<dynamic>.from(restaurants.map((x) => x)),
    "is_valid": isValid == null ? null : isValid,
    "share_url": shareUrl == null ? null : shareUrl,
    "show_share_url": showShareUrl == null ? null : showShareUrl,
    "title": title == null ? null : title,
    "description": description == null ? null : description,
    "display_time": displayTime == null ? null : displayTime,
    "display_date": displayDate == null ? null : displayDate,
    "is_end_time_set": isEndTimeSet == null ? null : isEndTimeSet,
    "disclaimer": disclaimer == null ? null : disclaimer,
    "event_category": eventCategory == null ? null : eventCategory,
    "event_category_name": eventCategoryName == null ? null : eventCategoryName,
    "book_link": bookLink == null ? null : bookLink,
    "types": types == null ? null : List<dynamic>.from(types.map((x) => x)),
    "share_data": shareData == null ? null : shareData.toJson(),
  };
}

class PhotoElement {
  PhotoElement({
    this.photo,
  });

  PhotoPhoto photo;

  factory PhotoElement.fromJson(Map<String, dynamic> json) => PhotoElement(
    photo: json["photo"] == null ? null : PhotoPhoto.fromJson(json["photo"]),
  );

  Map<String, dynamic> toJson() => {
    "photo": photo == null ? null : photo.toJson(),
  };
}

class PhotoPhoto {
  PhotoPhoto({
    this.url,
    this.thumbUrl,
    this.order,
    this.md5Sum,
    this.id,
    this.photoId,
    this.uuid,
    this.type,
  });

  String url;
  String thumbUrl;
  int order;
  String md5Sum;
  int id;
  int photoId;
  int uuid;
  String type;

  factory PhotoPhoto.fromJson(Map<String, dynamic> json) => PhotoPhoto(
    url: json["url"] == null ? null : json["url"],
    thumbUrl: json["thumb_url"] == null ? null : json["thumb_url"],
    order: json["order"] == null ? null : json["order"],
    md5Sum: json["md5sum"] == null ? null : json["md5sum"],
    id: json["id"] == null ? null : json["id"],
    photoId: json["photo_id"] == null ? null : json["photo_id"],
    uuid: json["uuid"] == null ? null : json["uuid"],
    type: json["type"] == null ? null : json["type"],
  );

  Map<String, dynamic> toJson() => {
    "url": url == null ? null : url,
    "thumb_url": thumbUrl == null ? null : thumbUrl,
    "order": order == null ? null : order,
    "md5sum": md5Sum == null ? null : md5Sum,
    "id": id == null ? null : id,
    "photo_id": photoId == null ? null : photoId,
    "uuid": uuid == null ? null : uuid,
    "type": type == null ? null : type,
  };
}

class ShareData {
  ShareData({
    this.shouldShow,
  });

  int shouldShow;

  factory ShareData.fromJson(Map<String, dynamic> json) => ShareData(
    shouldShow: json["should_show"] == null ? null : json["should_show"],
  );

  Map<String, dynamic> toJson() => {
    "should_show": shouldShow == null ? null : shouldShow,
  };
}

class Popularity {
  Popularity({
    this.popularity,
    this.nightlifeIndex,
    this.nearbyRes,
    this.topCuisines,
    this.popularityRes,
    this.nightlifeRes,
    this.subzone,
    this.subzoneId,
    this.city,
  });

  String popularity;
  String nightlifeIndex;
  List<String> nearbyRes;
  List<String> topCuisines;
  String popularityRes;
  String nightlifeRes;
  String subzone;
  int subzoneId;
  String city;

  factory Popularity.fromJson(Map<String, dynamic> json) => Popularity(
    popularity: json["popularity"] == null ? null : json["popularity"],
    nightlifeIndex: json["nightlife_index"] == null ? null : json["nightlife_index"],
    nearbyRes: json["nearby_res"] == null ? null : List<String>.from(json["nearby_res"].map((x) => x)),
    topCuisines: json["top_cuisines"] == null ? null : List<String>.from(json["top_cuisines"].map((x) => x)),
    popularityRes: json["popularity_res"] == null ? null : json["popularity_res"],
    nightlifeRes: json["nightlife_res"] == null ? null : json["nightlife_res"],
    subzone: json["subzone"] == null ? null : json["subzone"],
    subzoneId: json["subzone_id"] == null ? null : json["subzone_id"],
    city: json["city"] == null ? null : json["city"],
  );

  Map<String, dynamic> toJson() => {
    "popularity": popularity == null ? null : popularity,
    "nightlife_index": nightlifeIndex == null ? null : nightlifeIndex,
    "nearby_res": nearbyRes == null ? null : List<dynamic>.from(nearbyRes.map((x) => x)),
    "top_cuisines": topCuisines == null ? null : List<dynamic>.from(topCuisines.map((x) => x)),
    "popularity_res": popularityRes == null ? null : popularityRes,
    "nightlife_res": nightlifeRes == null ? null : nightlifeRes,
    "subzone": subzone == null ? null : subzone,
    "subzone_id": subzoneId == null ? null : subzoneId,
    "city": city == null ? null : city,
  };
}
