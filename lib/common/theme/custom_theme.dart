import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iamsng/common/constants/dimens_constants.dart';
import 'package:iamsng/common/constants/font_constants.dart';





final ThemeData myCustomTheme = new ThemeData(
  primaryColor: CustomTheme.colorPrimary,
  backgroundColor: CustomTheme.colorWhite,

  scaffoldBackgroundColor: CustomTheme.colorWhite,
  cursorColor: CustomTheme.colorBotton,
  buttonTheme: CustomTheme._btnThemeBlue,
  appBarTheme: AppBarTheme(
    color: CustomTheme.colorGradientTop
  ),

  accentColor: CustomTheme.colorGradientBottom,
  primaryTextTheme: TextTheme(
    headline: CustomTheme.commonHeadTextStyle,
    subtitle: CustomTheme.commonSubTitleTextStyle,
    title:CustomTheme.commonTitleTextStyle

  )
);



class CustomTheme {


  static const Color colorPrimary = Color(0xFF25282B);
  static const Color colorWhite = Colors.white;
  static const Color colorGreyText = Color(0xFF52575C);
  static const Color colorGreyBackground = Color(0xFFE8E8E8);
  static const Color colorGradientTop =  Color(0xFFaa91be);
  static const Color colorBotton =  Color(0xFFac7c94);
  static const Color colorGradientBottom =  Color(0xFFf7d8c7);


  static const Map<int, Color> GREY = const <int, Color>{
    400: const Color(0xFFbdbdbd),
    500: const Color(0xFF9e9e9e),
    600: const Color(0xFF757575),
    700: const Color(0xFF616161),
    800: const Color(0xFF424242),
    900: const Color(0xFF212121),
  };




  static final TextStyle buttonTextStyle = TextStyle(color: colorWhite, fontSize: FONT_SIZE_20, fontFamily: ROBOTO, fontWeight: ROBOTO_BOLD_WEIGHT);

  static final ButtonThemeData _btnThemeBlue = ButtonThemeData(
    buttonColor: colorPrimary,
    disabledColor: colorGreyBackground,
    height: BUTTON_HEIGHT_30,
    padding:  EdgeInsets.all(PADDING_15),
    textTheme: ButtonTextTheme.accent,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(BORDER_04)),
  );




  static RoundedRectangleBorder circularBorderWithRadius = RoundedRectangleBorder(side: BorderSide(color: colorGreyText, style: BorderStyle.solid, width: BORDER_01), borderRadius: BorderRadius.circular(BORDER_02));


//----------------------------------------------------------------------Common Text Style----------------------------------------------------------------------------------------------------------------

  static final TextStyle commonHeadTextStyle = TextStyle(color: colorGreyText, fontSize: FONT_SIZE_20, fontFamily: ROBOTO, fontWeight: ROBOTO_BOLD_WEIGHT);
  static final TextStyle commonTitleTextStyle = TextStyle(color: colorGreyText, fontSize: FONT_SIZE_18, fontFamily: ROBOTO, fontWeight: ROBOTO_BOLD_WEIGHT);
  static final TextStyle commonSubTitleTextStyle = TextStyle(color: colorGreyText, fontSize: FONT_SIZE_16, fontFamily: ROBOTO, fontWeight: ROBOTO_REGULAR_WEIGHT);

//-------------------------------------------------------------------------Title-------------------------------------------------------------------------------------------------------------

  /*--------------------------------------------------------COLOR GREY TEXT----------------------------------------------------------------*/

  //---------------------TEXT STYLE MEDIUM WITH COLOR GREY TEXT--------------------------//
  static final TextStyle textStyle_medium_color_greyText_14 = TextStyle(color: colorGreyText, fontSize: FONT_SIZE_14, fontFamily: ROBOTO, fontWeight: ROBOTO_MEDIUM_WEIGHT);


}
