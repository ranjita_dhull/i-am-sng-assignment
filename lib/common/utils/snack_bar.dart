import 'package:flutter/material.dart';
import 'package:iamsng/common/theme/custom_theme.dart';
import 'package:iamsng/custom_widgets/custom_text.dart';



class CustomSnackBar {
  BuildContext context;
  String message = "";
  var scaffoldKey;

  CustomSnackBar({this.context, this.message, this.scaffoldKey});

  showSnackBar() {
    final snackbar = new SnackBar(content: new CustomText(message,textstyle: CustomTheme.textStyle_medium_color_greyText_14,),backgroundColor: CustomTheme.colorPrimary,);
    Scaffold.of(context).showSnackBar(snackbar);
  }
}
