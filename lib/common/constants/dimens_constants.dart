//  used for images
const double IMAGE_HEIGHT_20 = 20.0;
const double IMAGE_HEIGHT_25 = 25.0;
const double IMAGE_HEIGHT_30 = 30.0;
const double IMAGE_HEIGHT_40 = 40.0;
const double IMAGE_HEIGHT_65 = 65.0;
const double IMAGE_WIDTH_65 = 65.0;
const double IMAGE_WIDTH_40 = 40.0;
const double IMAGE_WIDTH_25 = 25.0;

const double IMAGE_HEIGHT_50 = 50.0;
const double IMAGE_WIDTH_50 = 50.0;
const double IMAGE_HEIGHT_100 = 100.0;
const double IMAGE_HEIGHT_150 = 150.0;
const double IMAGE_WIDTH_100 = 100.0;
const double IMAGE_WIDTH_150 = 150.0;
const double IMAGE_HEIGHT_80 = 80.0;
const double IMAGE_WIDTH_80 = 80.0;
const double IMAGE_WIDTH_120 = 120.0;
const double IMAGE_WIDTH_20 = 20.0;
const double IMAGE_WIDTH_15 = 15.0;
const double IMAGE_HEIGHT_15 = 15.0;
const double IMAGE_HEIGHT_16 = 16.0;
const double IMAGE_HEIGHT_13 = 13.0;
const double IMAGE_WIDTH_13 = 13.0;
const double IMAGE_WIDTH_16 = 16.0;


const double IMAGE_HEIGHT_120 = 120.0;
const double IMAGE_HEIGHT_130 = 130.0;
const double IMAGE_WIDTH_70 = 70.0;
const double IMAGE_WIDTH_130 = 130.0;
const double IMAGE_WIDTH_250 = 250.0;
const double IMAGE_HEIGHT_70 = 70.0;

//  used for margin
const double MARGIN_0 = 0.0;
const double MARGIN_2 = 2.0;
const double MARGIN_3 = 3.0;
const double MARGIN_8 = 8.0;
const double MARGIN_6 = 6.0;
const double MARGIN_5 = 5.0;
const double MARGIN_10 = 10.0;
const double MARGIN_15 = 15.0;
const double MARGIN_20 = 20.0;
const double MARGIN_25 = 25.0;
const double MARGIN_22 = 22.0;
const double MARGIN_30 = 30.0;
const double MARGIN_40 = 40.0;
const double MARGIN_50 = 50.0;
const double MARGIN_60 = 60.0;

//  used for padding
const double PADDING_0 = 0.0;
const double PADDING_1 = 1.0;
const double PADDING_2 = 2.0;
const double PADDING_3 = 3.0;
const double PADDING_4 = 4.0;
const double PADDING_5 = 5.0;
const double PADDING_8 = 8.0;
const double PADDING_10 = 10.0;
const double PADDING_12 = 12.0;
const double PADDING_15 = 15.0;
const double PADDING_16 = 16.0;
const double PADDING_20 = 20.0;
const double PADDING_25 = 25.0;
const double PADDING_24 = 24.0;
const double PADDING_30 = 30.0;
const double PADDING_40 = 40.0;
const double PADDING_50 = 50.0;
const double PADDING_60 = 60.0;
const double PADDING_80 = 80.0;
const double PADDING_17 = 17.0;

//  used for font
const double FONT_SIZE_5 = 5.0;
const double FONT_SIZE_6 = 6.0;
const double FONT_SIZE_8 = 8.0;
const double FONT_SIZE_10 = 10.0;
const double FONT_SIZE_11 = 11.0;
const double FONT_SIZE_12 = 12.0;
const double FONT_SIZE_13 = 13.0;
const double FONT_SIZE_14 = 14.0;
const double FONT_SIZE_21 = 21.0;
const double FONT_SIZE_15 = 15.0;
const double FONT_SIZE_16 = 16.0;
const double FONT_SIZE_17 = 17.0;
const double FONT_SIZE_18 = 18.0;
const double FONT_SIZE_20 = 20.0;
const double FONT_SIZE_22 = 22.0;
const double FONT_SIZE_24 = 24.0;
const double FONT_SIZE_40 = 40.0;
const double FONT_SIZE_25 = 25.0;
const double FONT_SIZE_30 = 30.0;
const double FONT_SIZE_32 = 32.0;
const double FONT_SIZE_35 = 35.0;
const double FONT_SIZE_50 = 50.0;
const double FONT_SIZE_70 = 70.0;
const double FONT_SIZE_80 = 80.0;
const double FONT_SIZE_120 = 120.0;

// used for the stack positions
const double POSITION_0 = 0.0;
const double POSITION_5 = 5.0;
const double POSITION_8 = 8.0;
const double POSITION_MINUS_3 = -3.0;
const double POSITION_10 = 10.0;
const double POSITION_20 = 20.0;
const double POSITION_30 = 30.0;
const double POSITION_40 = 40.0;
const double POSITION_55 = 55.0;

// used for icon size
const double ICON_SIZE_10 = 10.0;
const double ICON_SIZE_15 = 15.0;
const double ICON_SIZE_8 = 8.0;
const double ICON_SIZE_5 = 5.0;
const double ICON_SIZE_20 = 20.0;
const double ICON_SIZE_30 = 30.0;
const double ICON_SIZE_18 = 18.0;
const double ICON_SIZE_14 = 14.0;
const double ICON_SIZE_25 = 25.0;
const double ICON_SIZE_22 = 22.0;
const double ICON_SIZE_24 = 24.0;
const double ICON_SIZE_40 = 40.0;
const double ICON_SIZE_35 = 35.0;
const double ICON_SIZE_60 = 60.0;
const double ICON_SIZE_50 = 50.0;
const double ICON_SIZE_70 = 70.0;
const double ICON_SIZE_80 = 80.0;
const double ICON_SIZE_90 = 90.0;
const double ICON_SIZE_110 = 110.0;

// used for the media query dimens
const double MEDIA_QUERY_3_0 = 3.0;
const double MEDIA_QUERY_2_7 = 2.7;
const double MEDIA_QUERY_5_0 = 5.0;
const double MEDIA_QUERY_1_5 = 1.5;


// Used for the smooth rating bar
const double SMOOTH_RATING_BAR_SIZE_20 = 20.0;
const double SMOOTH_RATING_BAR_SIZE_40 = 40.0;

// Used for the container size
const double CONTAINER_WIDTH_2 = 2.0;
const double CONTAINER_WIDTH_1 = 1.0;
const double CONTAINER_HEIGHT_2 = 2.0;
const double CONTAINER_HEIGHT_1 = 1.0;
const double CONTAINER_HEIGHT_4 = 4.0;
const double CONTAINER_HEIGHT_24 = 24.0;
const double CONTAINER_HEIGHT_36 = 36.0;
const double CONTAINER_WIDTH_24 = 24.0;
const double CONTAINER_WIDTH_30 = 30.0;
const double CONTAINER_WIDTH_20 = 20.0;
const double CONTAINER_WIDTH_25 = 25.0;
const double CONTAINER_HEIGHT_15 = 15.0;
const double CONTAINER_HEIGHT_10 = 10.0;
const double CONTAINER_HEIGHT_20 = 20.0;
const double CONTAINER_HEIGHT_25 = 25.0;
const double CONTAINER_HEIGHT_30 = 30.0;
const double CONTAINER_WIDTH_50 = 50.0;
const double CONTAINER_HEIGHT_45 = 45.0;
const double CONTAINER_HEIGHT_50 = 50.0;
const double CONTAINER_HEIGHT_65 = 65.0;
const double CONTAINER_HEIGHT_40 = 40.0;
const double CONTAINER_WIDTH_60 = 60.0;
const double CONTAINER_HEIGHT_60 = 60.0;
const double CONTAINER_WIDTH_70 = 70.0;
const double CONTAINER_HEIGHT_70 = 70.0;
const double CONTAINER_HEIGHT_85 = 85.0;
const double CONTAINER_HEIGHT_80 = 80.0;
const double CONTAINER_WIDTH_85 = 85.0;
const double CONTAINER_WIDTH_80 = 80.0;
const double CONTAINER_WIDTH_100 = 100.0;
const double CONTAINER_WIDTH_400 = 400.0;
const double CONTAINER_WIDTH_40 = 40.0;
const double CONTAINER_HEIGHT_100 = 100.0;
const double CONTAINER_HEIGHT_400 = 400.0;
const double CONTAINER_HEIGHT_120 = 120.0;
const double CONTAINER_HEIGHT_140 = 140.0;
const double CONTAINER_HEIGHT_170 = 170.0;
const double CONTAINER_HEIGHT_190 = 190.0;
const double CONTAINER_HEIGHT_280 = 280.0;
const double CONTAINER_WIDTH_120 = 120.0;
const double CONTAINER_WIDTH_140 = 140.0;
const double CONTAINER_WIDTH_300 = 300.0;
const double CONTAINER_HEIGHT_300 = 300.0;
const double CONTAINER_HEIGHT_150 = 150.0;
const double CONTAINER_WIDTH_15 = 15.0;
const double CONTAINER_HEIGHT_350 = 350.0;
const double CONTAINER_HEIGHT_370 = 370.0;
const double CONTAINER_HEIGHT_250 = 250.0;
const double CONTAINER_HEIGHT_200 = 200.0;
const double CONTAINER_HEIGHT_105 = 105.0;
const double CONTAINER_HEIGHT_600 = 600.0;
const double CONTAINER_HEIGHT_500 = 500.0;


//common dimentions
const double SCALE_FACTOR_04 = 0.4;
const double SCALE_FACTOR_08 = 0.8;

// circle indicator
const double CIRCLE_INDICATOR_WIDTH_7 = 7.0;
const double CIRCLE_INDICATOR_HEIGHT_7 = 7.0;
const double CIRCLE_INDICATOR_WIDTH_35 = 35.0;
const double CIRCLE_INDICATOR_HEIGHT_35 = 35.0;

// circle radius
const double CIRCLE_RADIUS_0 = 0.0;
const double CIRCLE_RADIUS_5 = 5.0;
const double CIRCLE_RADIUS_1 = 1.0;
const double CIRCLE_RADIUS_7 = 7.0;
const double CIRCLE_RADIUS_15 = 15.0;
const double CIRCLE_RADIUS_32 = 32.0;

const double CIRCLE_RADIUS_10 = 10.0;
const double CIRCLE_RADIUS_14 = 14.0;
const double CIRCLE_RADIUS_12 = 12.0;
const double CIRCLE_RADIUS_20 = 20.0;
const double CIRCLE_RADIUS_30 = 30.0;
const double CIRCLE_RADIUS_40 = 40.0;
const double CIRCLE_RADIUS_50 = 50.0;
const double CIRCLE_RADIUS_60 = 60.0;

// sized box
const double SIZED_BOX_30 = 30.0;
const double SIZED_BOX_35 = 35.0;
const double SIZED_BOX_40 = 40.0;
const double SIZED_BOX_100 = 100.0;
const double SIZED_BOX_70 = 70.0;
const double SIZED_BOX_50 = 50.0;
const double SIZED_BOX_60 = 60.0;
const double SIZED_BOX_120 = 120.0;
const double SIZED_BOX_25 = 25.0;
const double SIZED_BOX_20 = 20.0;
const double SIZED_BOX_15 = 15.0;
const double SIZED_BOX_12 = 12.0;
const double SIZED_BOX_10 = 10.0;
const double SIZED_BOX_16 = 16.0;
const double SIZED_BOX_5 = 5.0;
const double SIZED_BOX_2 = 2.0;
const double SIZED_BOX_1 = 1.0;
const double SIZED_BOX_0 = 0.0;

// opacity
const double OPACITY_07 = 0.7;
const double OPACITY_05 = 0.5;
const double OPACITY_02 = 0.2;
const double OPACITY_1 = 1.0;
// elevation
const double ELEVATION_0_5 = 0.5;
const double ELEVATION_05 = 5.0;
const double ELEVATION_10 = 10.0;
const double ELEVATION_20 = 20.0;
const double ELEVATION_03 = 3.0;
const double ELEVATION_02 = 2.0;
const double ELEVATION_01 = 1.0;
const double ELEVATION_00 = 0.0;

// divider
const double DIVIDER_01 = 1.0;
const double DIVIDER_02 = 2.0;
const double DIVIDER_05 = 5.0;
//stack
const double STACK_ALIGNEMT = 1.0;
const double STACK_ALIGNEMT_08 = 0.8;

// card height (Home Screen)
const double CARD_HEIGHT = 160.0;
const double CARD_HEIGHT_80 = 80.0;

//  Calendar Height
const double CALENDAR_HEIGHT_360 = 360.0;

// divider
const double BORDER_0_3 = 0.3;
const double BORDER_01 = 1.0;
const double BORDER_02 = 2.0;
const double BORDER_06 = 6.0;
const double BORDER_04 = 4.0;
const double BORDER_0_5 = 0.5;


//checkbox scale size
const double CHECKBOX_SCALE_SIZE = 0.9;

//Button Height
const double BUTTON_HEIGHT_50=50.0;
const double BUTTON_HEIGHT_30=30.0;


const double SCALE_1 = 1.0;
const double SIZE_1 = 1.0;


