import 'package:flutter/material.dart';

const String ROBOTO = 'Roboto';
const FontWeight ROBOTO_BOLD_WEIGHT = FontWeight.w700;
const FontWeight ROBOTO_REGULAR_WEIGHT = FontWeight.w400;
const FontWeight ROBOTO_MEDIUM_WEIGHT = FontWeight.w500;
