import 'dart:async';


import 'package:iamsng/model/restaurent_response_model.dart';
import 'package:iamsng/state/repository_state.dart';
import 'package:rxdart/subjects.dart';

//  It handles all the data in one class
class RepositoryBloc {
  static RepositoryBloc REPOSITORY_BLOC;

  static RepositoryBloc getInstance() {
    if (REPOSITORY_BLOC == null) {
      REPOSITORY_BLOC = new RepositoryBloc();
    }

    return REPOSITORY_BLOC;
  }

  // create an instance of setting  state
  final repositoryResponseState = RepositoryState();


  // it used to update the data in app state
  final _repositoryBehavior = BehaviorSubject<RepositoryState>();


  // used to perform action need on state
  final _repositoryUpdateController = StreamController<RepositoryState>(); // update
  final _repositoryRefreshController = StreamController<RepositoryState>(); // update



  // init the constructor
  RepositoryBloc() {
    // setting
    _repositoryUpdateController.stream.listen(_actionUpdateRepository);
    _repositoryRefreshController.stream.listen(_refreshRepository);
  }

  void dispose() {

    repositoryResponseState.clear();

  }

  void disposeControllers() {
    _repositoryUpdateController.close();
  }

  // stream
  // appstate
  Stream<RepositoryState> get repositoryState {
    return _repositoryBehavior.stream;
  }

  // used to update repository as a interface
  Sink<RepositoryState> get updateRespository {
    return _repositoryUpdateController.sink;
  }

  // used to perform on repository
  _actionUpdateRepository(RepositoryState _repositoryState) {

    //bifercate cusine classification
    _repositoryState.responseModel.popularity.topCuisines.forEach((cuisines){
      CuisineWiseClassification cuisineWiseClassification=CuisineWiseClassification();
      _repositoryState.responseModel.nearbyRestaurants.forEach((nearByRestaurents) {
        List<String> cuisinesList=nearByRestaurents.restaurant.cuisines.split(',');

        for (int i = 0; i < cuisinesList.length; i++)
          cuisinesList[i] = cuisinesList[i].toLowerCase().trim();

        if(cuisinesList.contains(cuisines.toLowerCase().trim())){
          cuisineWiseClassification.nearbyRestaurants.add(nearByRestaurents);
        }

      });

      cuisineWiseClassification.popularRestaurants.clear();
      cuisineWiseClassification.popularRestaurants.addAll(cuisineWiseClassification.nearbyRestaurants);
      cuisineWiseClassification.popularRestaurants
          .sort((a, b) =>
          num.parse(b.restaurant.userRating.aggregateRating).compareTo(
              num.parse(a.restaurant.userRating.aggregateRating)));

      repositoryResponseState.cuisineWiseClassificationList.add(cuisineWiseClassification);
    });


    //sort popular restaurents
    repositoryResponseState.popularRestaurants.clear();

      repositoryResponseState.popularRestaurants.addAll(
          _repositoryState.responseModel.nearbyRestaurants);
      repositoryResponseState.popularRestaurants
          .sort((a, b) =>
          num.parse(b.restaurant.userRating.aggregateRating).compareTo(
              num.parse(a.restaurant.userRating.aggregateRating)));


    repositoryResponseState.responseModel=_repositoryState.responseModel;
    repositoryResponseState.status=_repositoryState.status;
    _repositoryBehavior.add(repositoryResponseState);
  }


  // used to refresh repository as a interface
  Sink<RepositoryState> get refreshRespository {
    return _repositoryRefreshController.sink;
  }

  // used to refresh on repository
  _refreshRepository(RepositoryState _repositoryState) {

    repositoryResponseState.responseModel=_repositoryState.responseModel;
    repositoryResponseState.status=_repositoryState.status;
    repositoryResponseState.popularRestaurants.clear();
    repositoryResponseState.popularRestaurants.addAll(_repositoryState.popularRestaurants);
    _repositoryBehavior.add(repositoryResponseState);
  }

}

